import React, { Component } from "react";
import { data } from "./data";
import ItemGlass from "./ItemGlass";
import Header from "./Header";
import Model from "./Model";
import CSS from "./baiTapGlasses.module.css";

class BaiTapGlasses extends Component {
  state = {
    glasses: data,
    detailGlasse: data[0],
  };

  renderListGlasses = () => {
    return this.state.glasses.map((item, index) => {
      return (
        <ItemGlass
          key={index}
          data={item}
          handleChangeGlasses={this.handleChangeGlasses}
        />
      );
    });
  };

  handleChangeGlasses = (glass) => {
    this.setState({
      detailGlasse: glass,
    });
  };

  render() {
    return (
      <div id={CSS.content}>
        <div id={CSS.header} className="bg-dark text-white py-4">
          <Header />
        </div>
        <div id={CSS.model}>
          <Model model={this.state.detailGlasse} />
        </div>
        <div id={CSS.list__Glasses} className="container">
          {this.renderListGlasses()}
        </div>
      </div>
    );
  }
}

export default BaiTapGlasses;
