import React, { Component } from "react";

class ItemGlass extends Component {
  render() {
    let { data } = this.props;
    return (
      <div>
        <img
          onClick={() => this.props.handleChangeGlasses(data)}
          style={{ width: "130px", padding: "0 15px" }}
          src={data.url}
          alt=""
        />
      </div>
    );
  }
}

export default ItemGlass;
