import React, { Component } from "react";
import CSS from "./baiTapGlasses.module.css";

class Model extends Component {
  render() {
    // console.log(this.props);
    let { model } = this.props;
    return (
      <div id={CSS.img__Model}>
        <img style={{ width: "185px" }} src="./glassesImage/model.jpg" />
        <img id={CSS.img__Glass} src={model.url} alt="" />
        <p id={CSS.info__Glass}>
          <span id={CSS.name}>{model.name}</span> <br />
          <span>{model.desc}</span>
        </p>
      </div>
    );
  }
}

export default Model;
